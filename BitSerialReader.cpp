#include "BitSerialReader.h"

BitSerialReader::BitSerialReader(HardwareSerial *serial) {
    this->serial = serial;
}

bool BitSerialReader::available() {
    return (serial -> available() || mask != 1);
}

bool BitSerialReader::read() {
    if (mask != 1) {
        mask >>= 1;
        return (byte_buf & mask) == mask;
    } else {
        mask = 256;
        while (! serial->available());
        byte_buf = serial->read();
        return read();
    }
}