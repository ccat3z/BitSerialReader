#include <BitSerialReader.h>

// init the reader
BitSerialReader bsr(&Serial);

void setup() {
    Serial.begin(9600);
    while (!Serial); // wait the serial ready
    pinMode(13, OUTPUT);
}

void loop() {
    while (!bsr.available());

    // control the led 13
    // 2s for 1
    // 0.5s for 0
    digitalWrite(13, HIGH);
    delay(bsr.read() ? 2000 : 500);
    digitalWrite(13, LOW);
    delay(1000);
}