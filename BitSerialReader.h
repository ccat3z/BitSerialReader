#ifndef BIT_SERIAL_READER_H
#include <Arduino.h>
#include <HardwareSerial.h>

// read by byte from hardware serial
class BitSerialReader {
    private:
        int byte_buf = -1;
        int mask = 1;
        HardwareSerial *serial;
    public:
        BitSerialReader(HardwareSerial *serial);
        bool available();
        bool read();
};

#endif